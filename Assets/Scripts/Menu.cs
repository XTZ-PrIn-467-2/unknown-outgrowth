﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Menu : MonoBehaviour {

    public Canvas MainCanvas;
    public Canvas MenuCanvas;
    public Canvas PersonageCanvas;
    public Scrollbar scrollbar;
    public Canvas HistoryCanvas;
    public Canvas InformationCanvas;

    public void Awake()
    {
        MenuCanvas.enabled = false;
        PersonageCanvas.enabled = false;
        HistoryCanvas.enabled = false;
        InformationCanvas.enabled = false;
    }



    public void MenuOn()
    {
        MenuCanvas.enabled = true;
        MainCanvas.enabled = false;
        PersonageCanvas.enabled = false;
        HistoryCanvas.enabled = false;
        InformationCanvas.enabled = false;
    }

    public void MenuContinue()
    {
        MenuCanvas.enabled = false;
        MainCanvas.enabled = true;
        PersonageCanvas.enabled = false;
        HistoryCanvas.enabled = false;
        InformationCanvas.enabled = false;
    }


    public void exitGame()
    {
        Application.Quit();
    }

    public void Personage()
    {
        MenuCanvas.enabled = false;
        MainCanvas.enabled = false;
        PersonageCanvas.enabled = true;
        HistoryCanvas.enabled = false;
        InformationCanvas.enabled = false;
    }

    public void HistorySettings()
    {
        MenuCanvas.enabled = false;
        MainCanvas.enabled = false;
        PersonageCanvas.enabled = false;
        HistoryCanvas.enabled = true;
        InformationCanvas.enabled = false;
    }

    public void InformationSettings()
    {
        MenuCanvas.enabled = false;
        MainCanvas.enabled = false;
        PersonageCanvas.enabled = false;
        HistoryCanvas.enabled = false;
        InformationCanvas.enabled = true;
    }
}
