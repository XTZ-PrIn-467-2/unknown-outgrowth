var searchData=
[
  ['identifier',['Identifier',['../class_quest.html#a6809d73cf80acff7d4d86daf6edc1bee',1,'Quest']]],
  ['instance',['Instance',['../class_alert_box.html#a2391d44a849d05a66ba21f88ee7bb555',1,'AlertBox.Instance()'],['../class_game_manager.html#ad3e717f4fb0f378b969f4457de81f23e',1,'GameManager.Instance()'],['../class_user.html#a36f745fa933006054b541386ab5597f4',1,'User.Instance()'],['../class_map.html#a3ebf2635745d1f61e88c2c98cf10c7b9',1,'Map.Instance()'],['../class_minimap.html#a9dcef1299aa42d5c2797ad038fca2332',1,'Minimap.Instance()']]],
  ['isleaf',['IsLeaf',['../class_dialog_tree_node.html#a75fcecdef245d68f08bdd767125190fb',1,'DialogTreeNode']]],
  ['isopen',['isOpen',['../class_tutorial.html#ac27b76d4b9efaa01ed2dc9798b5d2ddf',1,'Tutorial.isOpen()'],['../class_map.html#aeb1ce5aa250c6717aeffdb3e7f84d27b',1,'Map.IsOpen()'],['../class_minimap.html#aeb32f2373a10af3f74257c641acc0492',1,'Minimap.IsOpen()']]],
  ['isspeaking',['IsSpeaking',['../class_dialog_manager.html#a70256421677185e03e36261aa29d5638',1,'DialogManager']]],
  ['items',['Items',['../class_user.html#a90c02ee9c35e2b09281ba3fc17c989bc',1,'User']]]
];
