using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class PauseMenu : MonoBehaviour {
	/*
    *   Developed by: Duke
    *   Description: It's the pause menu. When user presses 'P', player is frozen and the menu appears. 
    * 				Player is able to select the options with the mouse. 
*/

	public GameObject pauseCanvas;
	public GameObject controlCanvas;
	public GameObject feedbackCanvas;
	private Stack<GameObject> allCanvas;
	private MenuStatus menuStatus;

	static PauseMenu instance;

	void Start() {
		instance = this;
		menuStatus = GameManager.Instance.menuStatus;
		allCanvas = new Stack<GameObject>();
		allCanvas.Push(pauseCanvas);
	}

	void Update () {
		if (Input.GetButtonDown ("Pause") && !menuStatus.openProblem("Pause")) {
			if (pauseCanvas.activeSelf) {
				allCanvas.Pop ().SetActive (false);
				if(allCanvas.Count == 1)
					menuStatus.close ("Pause");
			} else {
				pauseCanvas.SetActive (true);
				menuStatus.open ("Pause");
				allCanvas.Push (pauseCanvas);
			}
		}
	}

	public static PauseMenu Instance
	{
		get { return instance; }
	}

	public void openControl() {
		controlCanvas.SetActive (true);
		allCanvas.Push (controlCanvas);
		GameObject.Find ("upButton").GetComponent<Text> ().text = "W";
		GameObject.Find ("downButton").GetComponent<Text> ().text = "S";
		GameObject.Find ("leftButton").GetComponent<Text> ().text = "A";
		GameObject.Find ("rightButton").GetComponent<Text> ().text = "D";
		GameObject.Find ("runButton").GetComponent<Text> ().text = "LEFT SHIFT";
		GameObject.Find ("inventoryButton").GetComponent<Text> ().text = "Q";
		GameObject.Find ("pauseButton").GetComponent<Text> ().text = "P";
	}

	public void openFeedback(){
		menuStatus.open ("Feedback");
		feedbackCanvas.SetActive (true);
		allCanvas.Push (feedbackCanvas);
	}

	public void quitGame() {
		gameObject.AddComponent<SceneChanger>();
		SceneChanger sceneChanger = GetComponent<SceneChanger>();
		sceneChanger.destinyScene = "GameOpening";
		sceneChanger.Change();
		pauseCanvas.SetActive (false);
	}

	public void CloseFeedback() {
		this.allCanvas.Pop ().SetActive (false);
	}

	void OnDisable() {
		if(	GameObject.FindGameObjectWithTag ("Player") != null)
			GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController> ().OnPause (false);
	}

	void OnEnable() {
		if(	GameObject.FindGameObjectWithTag ("Player") != null)
			GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController> ().OnPause (false);
	}
}
