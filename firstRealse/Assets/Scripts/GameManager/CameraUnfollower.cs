using UnityEngine;
using System.Collections;

public class CameraUnfollower : MonoBehaviour
{
    
    CameraController cameraController;
    public bool unfollowX;
    public bool unfollowY;

	void Start ()
    {
        cameraController = Camera.main.GetComponent<CameraController>();
	}

    void OnTriggerStay2D(Collider2D col)
    {
        if(col.tag == "Player")
        {
            if (unfollowX) cameraController.followingPlayerX = false;
            if (unfollowY) cameraController.followingPlayerY = false;
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        if (col.tag == "Player")
        {
            if (unfollowX) cameraController.followingPlayerX = true;
            if (unfollowY) cameraController.followingPlayerY = true;
        }
    }
}
