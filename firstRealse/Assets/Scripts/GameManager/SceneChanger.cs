using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;


public class SceneChanger : MonoBehaviour 
{

	/*
		Developed by: Duke

		Description: This script is responsible for changing from one scene to another, making a FadeIn/FadeOut effect
		in the process.
	 */

	public string destinyScene = "";
    public string specificName = "";
	public bool locked = false;

    /// <summary>
    /// Calls the ScreenFader to fade in and then when it's done set's the player to spawn
    /// in the specific position related to the scene he is leaving.
    /// </summary>
	IEnumerator ChangeScene()
	{
        ScreenFader fader = GameObject.FindGameObjectWithTag("GameManager").GetComponent<ScreenFader>();
		yield return new WaitForSeconds (fader.BeginFade(1, 1.2f));

        string sceneName = SceneManager.GetActiveScene().name;
       
        if (specificName != "")
            sceneName = specificName;

		PlayerSpawn.SetTarget(sceneName);
		SceneManager.LoadScene (destinyScene);
	}

    public void Change()
    {
        StartCoroutine(ChangeScene());
    }

	void OnTriggerEnter2D(Collider2D collider)
	{
        if (!locked && collider.tag == "Player")
            Change();
	}
}
