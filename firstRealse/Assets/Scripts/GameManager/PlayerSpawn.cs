using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayerSpawn : MonoBehaviour 
{
	/*

		Developed by: Duke

		Description: This script is responsible for keeping track of the player positions when it is changing between scenes.
	 */

	static string leavingScene;

	/// <summary>
	/// Sets the current level the player is beforing moving for another scene
	/// </summary>
	/// <param name="currentLevel">The scene he is leaving</param>
	public static void SetTarget(string currentLevel)
	{
        leavingScene = currentLevel;
	}

	void OnEnable()
	{
		SceneManager.sceneLoaded += OnLevelFinishedLoading;
	}

	void OnDisable()
	{
		SceneManager.sceneLoaded -= OnLevelFinishedLoading;
	}
		
	void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
	{
		Transform target = null;
		if(GameObject.Find(leavingScene) != null) 
			target = GameObject.Find(leavingScene).GetComponent<Transform>();

		if(target != null)
		{
			Transform spawn = target.GetChild(0).GetComponent<Transform>();
			Transform player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
			Vector3 playerPosition = new Vector3(spawn.position.x, spawn.position.y, player.position.z);
			Vector3 cameraPosition = new Vector3(spawn.position.x, spawn.position.y, Camera.main.transform.position.z);
			player.position = playerPosition;
			Camera.main.transform.position = cameraPosition;
		}
	}
}
