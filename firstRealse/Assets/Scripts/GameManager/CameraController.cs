using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class CameraController : MonoBehaviour
{
    
    public bool followingPlayerX;
    public bool followingPlayerY;
    public float followSpeed = 7.5f;
    Transform player;

	void Start ()
    {
        followingPlayerX = true;
        followingPlayerY = true;
        if (GameObject.FindGameObjectWithTag("Player") != null)
            player = GameObject.FindGameObjectWithTag("Player").transform;
    }

	void OnEnable()
	{
		SceneManager.sceneLoaded += OnLevelFinishedLoading;
	}

	void OnDisable()
	{
		SceneManager.sceneLoaded -= OnLevelFinishedLoading;
	}

	void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
	{
		followingPlayerX = true;
		followingPlayerY = true;
		if(GameObject.FindGameObjectWithTag("Player") != null)
			player = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	void Update ()
    {
        if(player != null)
        {
            Vector3 target = transform.position;
            if (followingPlayerX) target.x = player.transform.position.x;
            if (followingPlayerY) target.y = player.transform.position.y;
            transform.position = Vector3.Lerp(transform.position, target, followSpeed * Time.deltaTime);
        }
	}
}
