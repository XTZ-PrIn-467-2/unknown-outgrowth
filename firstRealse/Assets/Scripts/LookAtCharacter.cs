using UnityEngine;
using System.Collections;


/*
 * USO: Use Este script para que o NPC olhe para o Character (player) quando iniciar uma conversa
 */

public class LookAtCharacter : MonoBehaviour {

	public Sprite lookingDown; 	
	public Sprite lookingUp; 	
	public Sprite lookingRight; 
	public Sprite lookingLeft;	
     Sprite origin;
     public bool backToOrigin = true;

	private SpriteRenderer spriteRenderer; 

	// Initilize conversation
	void OnTriggerStay2D(Collider2D objeto){
		if( Input.GetKey (KeyCode.Z) ){
			updateSprite(objeto);
		}
	}

    void OnTriggerExit2D(Collider2D objeto)
    {
        spriteRenderer.sprite = origin;
    }

	private void updateSprite(Collider2D objeto){
		float deltaX = objeto.transform.position.x - transform.position.x;
		float deltaY = objeto.transform.position.y - transform.position.y;

		if ( Mathf.Abs(deltaX) > Mathf.Abs(deltaY)) { 
			if( deltaX > 0){
				spriteRenderer.sprite = lookingRight;
			} else {
				spriteRenderer.sprite = lookingLeft;
			}
		} else { //character acima ou abaixo
			if( deltaY > 0){
				spriteRenderer.sprite = lookingUp;
			} else {
				spriteRenderer.sprite = lookingDown;
			}
		}
	}

	// Initialization
	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer>();
        origin = spriteRenderer.sprite;
	}
}
