using System;



/// <summary>
/// Developed by: Duke
/// Gerenic definiton of a PreCondition;
/// </summary>
public interface IPreCondition
{
	int identifier { get; }
	string name { get; set; }
	bool checkIfMatches(User userProfile);
}

