using System;
using System.Collections;
using UnityEngine.UI;
using UnityEngine;

/// <summary>
/// Developed by: Duke
/// Class that holds the iteration and properties of the Game Tutorial.
/// </summary>
public class Tutorial : MonoBehaviour
{
	/// <summary>
	/// Represents the main Text UI element.
	/// </summary>
	public Text mainText;

	/// <summary>
	/// Gets a value indicating whether this UI <see cref="Tutorial"/> is open.
	/// </summary>
	/// <value><c>true</c> if is open; otherwise, <c>false</c>.</value>
	public bool isOpen { get; private set; }

	/// <summary>
	/// The tutorial Canvas itself.
	/// </summary>
	public GameObject tutorialCanvas;

	/// <summary>
	/// The continue button.
	/// </summary>
	public Button continueBtn;

	/// <summary>
	/// Number of the text frame section.
	/// </summary>
	private int textPosition = 1;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake () 
	{
		if (!isOpen && this.checkIfAlertBoxIsOnScene() && !(GameManager.Instance.gameConfiguration.tutorialDone)) {
			this.OpenWindow ();
		}
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update(){
		if (!isOpen && tutorialCanvas.activeSelf) {
			tutorialCanvas.SetActive (false);
		}
	}
		
	/// <summary>
	/// Opens the window.
	/// </summary>
	public void OpenWindow()
	{
		this.tutorialCanvas.SetActive (true);
		isOpen = true;
	}

	/// <summary>
	/// Method that executes when the continue button is pressed.
	/// </summary>
	public void ContinuePressed(){
		if (textPosition < TUTORIAL_MESSAGES.Length && this.checkIfAlertBoxIsOnScene ()) {
			mainText.text = TUTORIAL_MESSAGES [textPosition];
			textPosition += 1;
		} else {
			tutorialCanvas.SetActive (false);
			GameManager.Instance.gameConfiguration.tutorialDone = true;
			isOpen = false;
		}
	}

	/// <summary>
	/// Checks if Tutorial is on scene.
	/// </summary>
	/// <returns><c>true</c>, if if tutorialCanvas is on scene, <c>false</c> otherwise.</returns>
	private bool checkIfAlertBoxIsOnScene(){
		if (tutorialCanvas == null)
			throw new NullReferenceException ();
		return true;
	}


	private string[] TUTORIAL_MESSAGES = {"SHerlock Holmes!\n\n" +
		"In the game, you have complete !\n\n" +
		"The project was developed by students of Poas group 467 and aims to be an Open Source platform/ or email me at!" +
		"https://www.facebook.com/dukechike?fref=ts" ,
		"Dont know the controls? Movement buttons are!\n\t\nWASD, Dont know directional Movements??o no jogo!\n\nRun Press o \"LEFT SHIFT\" press!\n " +
		"Press \"Z\" to interact with objects.\n" +
		"Press \"P\" press to access the pause menu.\n" +
		"Press \"Q\" to access quest menu.\n" +
		"Press \"I\" Press to access inventory menu.\n" +
		"Press \"M\" Press to mazimize the map.\n",
		"Now that you know the basics, you're ready and you can explore the game!\nClick continue to access the game!\n"};
}