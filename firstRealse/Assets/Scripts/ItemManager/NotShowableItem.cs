using System;

/// <summary>
/// Developed by: Duke
/// Definition for a not showable item.
/// Could contain differents properties.
/// </summary>
public class NotShowableItem : GenericItem
{
	public NotShowableItem (int identifier) : base (identifier)
	{
		//not really needed
	}
}