using UnityEngine;
using System.Collections;

public class Car : Mover
{
    /*
        Developed by: Duke

        Description: This is script is responsible for making cars move around in the CinParking scene.
        */

    [HideInInspector]
    public GameObject parkedAt;
  
    [HideInInspector]
    public bool parked = false;

    [HideInInspector]
    public bool unparking = false;

    protected override void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "DeadEnd")
        {
            spawner.spawnedAmount--;

            if (unparking)
            {
                if(spawner is CarSpawner)
                    ((CarSpawner)spawner).unparkingAmount--;
            }

            Destroy(this.gameObject);
        }
    }
}
