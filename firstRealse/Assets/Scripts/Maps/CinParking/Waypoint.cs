using UnityEngine;
using System.Collections;

public class Waypoint : MonoBehaviour
{
    /*
        Developed by: Duke

        Description: the waypoints are present in CinParking scene and are used for controlling the
        spots were the randomly generated cars can or cannot park, as also where the wanderers can travel.
    */

    public bool avaiable = true;
    public GameObject previous;

    public CarSpawner carSpawner;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Car" && this.tag == "ParkingSpot")
        {
            collider.GetComponent<Car>().parked = true;
            collider.GetComponent<Car>().parkedAt = this.gameObject;
            collider.GetComponent<AudioSource>().enabled = false;
            collider.GetComponentInChildren<AudioSource>().enabled = false;
            carSpawner.parkingAmount--;
            carSpawner.parkedAmount++;
        }
           
    }
}
