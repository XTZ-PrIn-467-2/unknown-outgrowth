using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoverSpawner : MonoBehaviour
{
    /*
        Developed by: Duke

        Description: This classe is reponsible for spawning objects with the Move script attached to it.
    */

    public List<GameObject> objects;
    public int spawnEverySeconds = 10;
    public GameObject targetWaypoint;

    [HideInInspector]
    public int spawnedAmount;

    protected float spawnTime;
    protected float spawn;

	protected virtual void Start ()
    {
        spawn = spawnEverySeconds + Random.Range(-spawnEverySeconds / 2f, spawnEverySeconds / 2f);
	}

    protected virtual void Update ()
    {
        spawnTime += Time.deltaTime;

        if (spawnTime > spawn)
            Spawn();
	}

    protected void GoForTargetWaypoint(Move move)
    {
        List<Vector2> path = new List<Vector2>();
        GameObject current = targetWaypoint;

        while(current != null)
        {
            path.Add(new Vector2(current.transform.position.x, current.transform.position.y));
            current = current.GetComponent<Waypoint>().previous;
        }

        for (int i = path.Count - 1; i >= 0; i--)
            move.addPoint(path[i]);
    }

    protected virtual void Spawn()
    {
        int r = Random.Range(0, objects.Count);
        GameObject newObject = Instantiate(objects[r], transform.position, Quaternion.identity) as GameObject;
        newObject.GetComponent<Mover>().spawner = this;
        newObject.transform.parent = this.transform;
        Move move = newObject.GetComponent<Move>();
        GoForTargetWaypoint(move);
        spawn = spawnEverySeconds + Random.Range(-spawnEverySeconds / 2f, spawnEverySeconds / 2f);
        spawnTime = 0;
    }
}
